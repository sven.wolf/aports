# Contributor: Sean McAvoy <seanmcavoy@gmail.com>
# Maintainer: Sean McAvoy <seanmcavoy@gmail.com>
pkgname=partclone
pkgver=0.3.22
pkgrel=0
pkgdesc="utilities to save and restore used blocks on a partition"
url="https://partclone.org"
arch="all"
license="GPL-2.0-only"
makedepends="
	autoconf
	automake
	bash
	btrfs-progs-dev
	diffutils
	docbook-xml
	e2fsprogs-dev
	f2fs-tools-dev
	gettext-dev
	hfsprogs
	intltool
	libtool
	libxslt-dev
	ncurses-dev
	ntfs-3g-dev
	openssl-dev
	"
subpackages="$pkgname-doc $pkgname-lang"
source="https://github.com/Thomas-Tsai/partclone/archive/$pkgver/partclone-$pkgver.tar.gz
	musl.patch
	very-funny-glibc-types.patch
	"

if [ "$CARCH" = "riscv64" ]; then
	options="$options textrels"
fi

prepare() {
	default_prepare
	autoreconf -fvi
}

build() {
	# xfs seems broken right now :(
	LIBS="-lintl" ./configure \
		--prefix=/usr \
		--disable-rpath \
		--enable-ncursesw \
		--enable-fat \
		--enable-extfs \
		--enable-exfat \
		--enable-ntfs \
		--enable-btrfs \
		--enable-minix \
		--enable-f2fs \
		--enable-hfsp \
		--enable-xfs
	make
}
check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

}

sha512sums="
324c1816a45540c81494b0ed31b1f54f80656cb4120bec2078a9d8af8d7c46af48d6c00b3057574ce77d61c20befa8740dcd765f7a7f6f8ff6970c942da5e376  partclone-0.3.22.tar.gz
0676b34c5818f8866e733b0aee151e8220b8db839ba732acfdaad4dcea81a4b5d514a520e43c79efcf53ba0788c9a7f06c043441b4d1f56dd684882329efce9d  musl.patch
16877cba85c0f2505940b88bf60b146b5412e624ab48733a5c9b1094f0a3ce031f8d3b5fd9aecd82fba6787f6939ae48fb713181d54db92990ff7b83e3b961c9  very-funny-glibc-types.patch
"
