# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=qqc2-desktop-style
pkgver=5.102.0
pkgrel=0
pkgdesc="A style for Qt Quick Controls 2 to make it follow your desktop theme"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-3.0-only AND (GPL-2.0-only OR GPL-3.0-only)"
depends="
	qt5-qtgraphicaleffects
	qt5-qtquickcontrols2
	"
depends_dev="
	kconfigwidgets-dev
	kiconthemes-dev
	kirigami2-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtx11extras-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/qqc2-desktop-style-$pkgver.tar.xz"
subpackages="$pkgname-dev"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ed11cd88b5bc1a311b757246ab6869e21983f443df8314d275ccf56e775ffb7e02529df496a46a822840fd70b1a904f364514959122a8d6c69d7de182481028b  qqc2-desktop-style-5.102.0.tar.xz
"
