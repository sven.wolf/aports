# Contributor: nibon7 <nibon7@163.com>
# Maintainer: nibon7 <nibon7@163.com>
pkgname=nushell
pkgver=0.74.0
pkgrel=0
pkgdesc="A new type of shell"
url="https://www.nushell.sh"
# s390x: nix crate
arch="all !s390x"
license="MIT"
makedepends="cargo openssl-dev>3 libx11-dev libxcb-dev libgit2-dev"
checkdepends="bash"
subpackages="$pkgname-plugins:_plugins"
install="$pkgname.post-install $pkgname.post-upgrade $pkgname.pre-deinstall"
source="$pkgname-$pkgver.tar.gz::https://github.com/nushell/nushell/archive/$pkgver.tar.gz"

# Reduce size of nu binary from 22.7 -> 13.8 MiB (on x86_64 with default feature).
export CARGO_PROFILE_RELEASE_LTO="fat"

# Temporarily allow textrels on riscv64
[ "$CARCH" = "riscv64" ] && options="$options textrels"

prepare() {
	default_prepare
	cargo fetch --locked
}

build() {
	cargo build --workspace --release --frozen
}

check() {
	cargo test --workspace --frozen
}

package() {
	find target/release \
		-maxdepth 1 \
		-executable \
		-type f \
		-name "nu*" \
		-exec install -Dm755 '{}' -t "$pkgdir"/usr/bin/ \;
}

_plugins() {
	pkgdesc="Nushell plugins"
	depends="nushell"

	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/nu_plugin_* "$subpkgdir"/usr/bin/
}

sha512sums="74d4801c7c968550830a9bf132388d614864d2449a53737409f7e18862fa3134a7e28bddd1b8482cf82fe9e39f25c238279b2a4ef9883594b5f4fef47bf9add2  nushell-0.74.0.tar.gz
"
